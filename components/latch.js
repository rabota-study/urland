AFRAME.registerComponent('latch', {
    schema: {
        target: {type: 'selector', default: 'a-camera'},
        positionTarget: {type: 'selector', default: '#magnet'},
        audioPlayer: {type: 'selector'},
        latched: {type: 'boolean', default: false},
        colorLatched: {default: 'blue'},
        color: {default: 'yellow'}
    },

    init: function () {
        this.directionVec3 = new THREE.Vector3();
        var self = this;
        var el = this.el;
        var scene = document.querySelector('a-scene');

        el.addEventListener('mouseenter', function () {
            // Change color
            el.setAttribute('color', self.data.colorLatched);

            // Set to latched
            self.data.latched = true;

            // Move this to the camera
            self.data.target.appendChild(el);

            // Place in position target
            var targetPosition = self.data.positionTarget.object3D.position;
            el.setAttribute('position', targetPosition);

            // Send event
            self.data.audioPlayer.components.sound.playSound();
        });

        el.addEventListener('mouseup', function() {
            if (self.data.latched) {
                // Change color back
                el.setAttribute('color', self.data.color);

                // Place object back
                scene.appendChild(el);

                // Place the position in it target
                var targetPosition = self.data.positionTarget.object3D.getWorldPosition();
                el.setAttribute('position', targetPosition);

                // Unlatch
                self.data.latched = false;
            }
        });
    }
});