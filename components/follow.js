AFRAME.registerComponent('follow', {
    schema: {
        target: {type: 'selector'},
        speed: {type: 'number'}
    },

    init: function () {
        this.directionVec3 = new THREE.Vector3();
        var self = this;
        var el = this.el;
    },

    tick: function (time, timeDelta) {
        // Grab position vectors (THREE.Vector3) from the entities' three.js objects.
        var targetPosition = this.data.target.object3D.getWorldPosition();

        // Translate the entity in the direction towards the target.
        this.el.setAttribute('position', targetPosition);
    }
});